(defmacro bind-to-function-name (fun-name body)
  `(defun ,fun-name (&rest args)
     (apply ,body args)))

(defun compose2 (f1 f2)
  (lexical-let ((f1 f1)
                (f2 f2))
    (lambda (&rest args)
      (funcall f1 (apply f2 args)))))

(defun colon-string-to-assoc (string)
  (let* ((splitted-line (split-string string ":"))
         (trimmed-splitted-line (mapcar (lambda (str) (string-trim str))
                                        splitted-line)))
    (cons (car trimmed-splitted-line) (cadr trimmed-splitted-line))))

(defun parse-response (response)
  (mapcar 'colon-string-to-assoc
          (remove-if (lambda (line) (not (find ?: line)))
                     (split-string response "\n"))))

;; Ante una respuesta de MPD setea la variable helm-mpd/search-results acorde a lo recibido
(defun handle-mpd-reply (process response)
  (let* ((parsed-response (parse-response response))
         (files (remove-if (lambda (elem) (not (equal "file" (car elem))))
                           parsed-response)))
    (setq helm-mpd/search-results files)))

(defun format-search-results (search-results)
  (mapcar #'cdr search-results))

;; Formatters
(defun send-string-to-mpd (s) (process-send-string mpd-stream s))
(defun mpd-search-format (search-term) (format "search any %S\n" search-term))
(defun mpd-fetch-format (file) (format "search file %S\n" file))
(defun mpd-add-format (file) (format "add %S\n" file))
(defun mpd-play-format (position) (format "play %d\n" position))

(bind-to-function-name mpd-search (compose2 'send-string-to-mpd 'mpd-search-format))
(bind-to-function-name mpd-fetch (compose2 'send-string-to-mpd 'mpd-fetch-format))
(bind-to-function-name mpd-add (compose2 'send-string-to-mpd 'mpd-add-format))
(bind-to-function-name mpd-play (compose2 'send-string-to-mpd 'mpd-play-format))
(defun mpd-playlist () (send-string-to-mpd "playlistinfo\n"))
(defun mpd-playlist-clear () (send-string-to-mpd "clear\n"))

;;;###autoload
(defun helm-mpd-search ()
  (progn
    (mpd-search helm-pattern)
    (format-search-results helm-mpd/search-results)))

;;;###autoload
(defvar helm-source-mpd-search
  '((name . "MPD")
    (volatile)
    (delayed)
    (candidates-process . helm-mpd-search)))

;;;###autoload
(defun helm-mpd ()
  (setq mpd-stream (open-network-stream "MPD" "*helm-mpd*" "localhost" 6600))
  (set-process-filter mpd-stream 'handle-mpd-reply)
  (interactive)
  (helm :sources '(helm-source-mpd-search)))

(provide 'helm-mpd)
