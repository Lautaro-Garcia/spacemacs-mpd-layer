(setq mpd-packages
      '((helm-mpd :location local)))

(defun mpd/init-helm-mpd ()
  (use-package helm-mpd
    :config (spacemacs/set-leader-keys "amms" 'helm-mpd)))
